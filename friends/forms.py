from django import forms

class Friend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }

    name_attrs = {
        'class': 'form-control',
        'placeholder':'Masukan nama temanmu disini'
    }

    url_attrs = {
        'class': 'form-control',
        'placeholder':'Masukan URL temanmu disini sesuai format'
    }

    name = forms.CharField(label = 'Nama',required=True,max_length=27,widget=forms.TextInput(attrs=name_attrs))
    url = forms.URLField(label = 'URL' , required=True, widget=forms.TextInput(attrs=url_attrs))
