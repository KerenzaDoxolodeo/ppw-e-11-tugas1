from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from datetime import datetime
from .forms import Friend_Form
from .models import Friend

# Create your views here.

def index(request):
    response = {'author' : 'Kelompok 11'}
    html = 'friendlist.html'
    #TODO Implement, isilah dengan 6 kata yang mendeskripsikan anda
    response['Friend_Form'] = Friend_Form
    message = Friend.objects.all()
    response['message'] = message
    return render(request, html, response)

def add_friend(request):
    response = {}
    form = Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        friend = Friend(name=form.cleaned_data['name'], url=form.cleaned_data['url'])
        friend.save()
        html = 'friendlist.html'
        return HttpResponseRedirect('/friends/')
    else:
        return HttpResponseRedirect('/friends/')

