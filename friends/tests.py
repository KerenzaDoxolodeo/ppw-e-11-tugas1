from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Friend
from .views import index, add_friend

# Create your tests here.
class AddProfileTest(TestCase):
    def test_friends_url_is_exist(self):
        response = Client().get('/friends/')
        self.assertEqual(response.status_code, 200)

    def test_friends_using_index_func(self):
        found = resolve('/friends/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_message(self):
        #Creating a new activity
        new_activity = Friend.objects.create(name="Robert" ,url='robert.herokuapp.com')

        #Retrieving all available activity
        counting_all_available_message= Friend.objects.all().count()
        self.assertEqual(counting_all_available_message,1)
    
    def test_friendlist_showing_all_messages(self):
        name_robert = 'Robert'
        url_robert = 'robert.herokuapp.com'
        data_robert = {'name': name_robert, 'url' : url_robert}
        post_data_robert = Client().post('/friends/add_friend', data_robert)
        self.assertEqual(post_data_robert.status_code, 302)

        response = Client().get('/friends/')
        html_response = response.content.decode('utf8')

        for key,data in data_robert.items():
            self.assertIn(data,html_response)

        self.assertIn(name_robert, html_response)
        self.assertIn(url_robert, html_response)

    def test_friendlist_can_handle_bad_input(self):
        name_bad  = ""
        url_bad = "boom"
        data_bad = {'name':name_bad, 'url':url_bad}
        post_data_bad = Client().post('/friends/add_friend',data_bad)

        response = Client().get('/friends')
        html_response = response.content.decode('utf8')

        self.assertNotIn("boom",html_response)
