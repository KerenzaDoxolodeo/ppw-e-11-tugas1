from django.db import models

# Create your models here.
class Profile(models.Model):
    Name = models.CharField(max_length=27)
    Birth_Date = models.CharField(max_length=14)
    Gender = models.CharField(max_length=10)
    Expertise = models.TextField()
    Description = models.CharField(max_length=30)
    Email = models.CharField(max_length=20)
