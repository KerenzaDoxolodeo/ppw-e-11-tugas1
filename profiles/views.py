from django.shortcuts import render
##from .models import Profile
name = 'John Doe'
bio = [
       {'subject': 'Birth Date',  'value': "1 Januari"},
       {'subject': 'Gender',      'value': 'Male'},
       {'subject': 'Description', 'value': 'nanti di edit'},
       {'subject': 'Email',       'value': 'john.doe@ui.ac.id'},
       ]
listexpertise = ['python','html','css']

# Create your views here.
def index(request):
##    profile = Profile.objects.first()
##    name = profile.Name
##    bio = [
##       {'subject': 'Name',  'value': profile.Name},
##       {'subject': 'Birth Date',  'value': profile.Birth_Date},
##       {'subject': 'Gender',      'value': profile.Gender},
##       {'subject': 'Expertise',   'value': profile.Expertise},
##       {'subject': 'Description', 'value': profile.Description},
##       {'subject': 'email',       'value': profile.Email},
##       ]
    response = {'bio': bio, 'name':name}
    response['expertise']= listexpertise
    return render(request, 'profilepage.html', response)
