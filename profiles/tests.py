from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

class Tugas1UnitTest(TestCase):

    def test_tugas_1_url_is_exist(self):
        response = Client().get('/profiles/')
        self.assertEqual(response.status_code, 200)

    def test_tugas1_using_index_func(self):
        found = resolve('/profiles/')
        self.assertEqual(found.func, index)

    def test_html_is_good(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode("utf-8")

        self.assertIn("1 Januari", html_response)
        self.assertIn("Male", html_response)
        self.assertIn("nanti di edit", html_response)
        self.assertIn("john.doe@ui.ac.id", html_response)

    def test_profile_test(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode("utf-8")

        self.assertIn("python", html_response)
        self.assertIn("html", html_response)
        self.assertIn("css", html_response)
