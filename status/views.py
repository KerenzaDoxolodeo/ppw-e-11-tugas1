from .models import StatusMessage
from .forms import Status_Form

from django.shortcuts import render
from datetime import datetime, date
from django.http import HttpResponseRedirect
respons = {}

def index(request):
    status = StatusMessage.objects.all()
    status = status.order_by('-created_date')

    respons['status'] = status
    respons['status_form'] = Status_Form
    html = 'status/status.html'
    return render(request, html, respons)

def add_message(request):
    print("HI")
    if 'error' in respons:
        del respons['error']
    #form = Status_Form(request.POST or None)
    if(request.method == 'POST'):
        respons['message'] = request.POST['message']
        if(0<len(respons['message'])<240):
            status = StatusMessage(message=respons['message'])
            status.save()
        elif len(respons['message'])==0:
            respons['error'] = 'Batas minimal pesan adalah 1 karakter'
        else:
            respons['error'] = 'Batas maksimal pesan adalah 240 karakter'
    return HttpResponseRedirect('/status/')
