# Create your models here.
from django.db import models
from django.utils import timezone
        
class StatusMessage(models.Model):
  
    message = models.CharField(max_length=240)
    created_date = models.DateTimeField(auto_now_add=True)
            
    def __str__(self):
        return self.message
