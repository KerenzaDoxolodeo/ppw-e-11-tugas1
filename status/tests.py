from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from datetime import date
from django.db import transaction
from .models import StatusMessage

# Ini buat tes fitur pipeline.
# Udah bisa dibuang
# ~ Deo

class StatusUnitTest(TestCase):

    def test_status_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code,200)

    def test_root_url_now_is_using_index_page_from_status(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response,'/status/',301,200)

    def test_model_can_create_new_message(self):
        #Creating a new activity
        with transaction.atomic():
            new_activity = StatusMessage.objects.create(message='This is a test')
            counting_all_available_message= StatusMessage.objects.all().count()
        self.assertEqual(counting_all_available_message,1)

    def test_status_post_success_and_render_the_result(self):
        message = 'Ini pesan'
        response = Client().post('/status/add_message', {'message': message})
        self.assertEqual(response.status_code, 302)
        response = Client().post('/status/')
        html_response = response.content.decode('utf8')
        self.assertIn(message,html_response)

    def test_model_can_be_string(self):
        new_activity = StatusMessage.objects.create(message='Pesan apakah aku?')
        self.assertEqual(str(new_activity),'Pesan apakah aku?') 

    def test_throw_error_when_status_is_too_long_or_short(self):
        message ='''Death doesn't discriminate
        Between the sinners and the saints,
        it takes and it takes and it takes
        and we keep living anyway.
        We rise and we fall
        and we break
        and we make our mistakes.
        And if there's a reason I'm still alive
        when everyone who loves me has died
        I'm willing to wait for it.
        I'm willing to wait for it.
        Wait for it

        Death doesn't discriminate
        Between the sinners and the saints
        It takes and it takes and it takes
        And we keep living anyway
        We rise and we fall
        And we break
        And we make our mistakes
        And if there's a reason I'm still alive
        When everyone who loves me has died
        I'm willing to wait for it (wait for it)
        I'm willing to wait for it

        '''
        Client().post('/status/add_message', {'message': message})
        response = Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertIn("Batas maksimal pesan adalah 240 karakter",html_response)

        message = ''
        Client().post('/status/add_message', {'message': message})
        response = Client().get('/status/')
        html_response = response.content.decode('utf8')

        self.assertIn("Batas minimal pesan adalah 1 karakter",html_response)
        self.assertIsNot("Batas maksimal pesan adalah 240 karakter",html_response)
        with transaction.atomic():
            new_activity = StatusMessage.objects.create(message='This is a test')
            counting_all_available_message= StatusMessage.objects.all().count()

        self.assertEqual(counting_all_available_message,1)

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class StatusFunctionalTest(TestCase):
    def setUp(self):

        chrome_options = Options()
        
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
    

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(StatusFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(StatusFunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/status/')
        # find the form element
        message = selenium.find_element_by_id('status-form-textarea')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        message.send_keys('Mengerjakan PR PPW 1')

        # submitting the form
        submit.send_keys(Keys.RETURN)