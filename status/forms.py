from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    message_attrs = {
        'type': 'text',
        'cols': 240,
        'rows': 6,
        'class': 'status-form-textarea',
        'id' : 'status-form-textarea',
        'placeholder':'Masukan status...'
    }

    message = forms.CharField(label='', required=True, max_length=240, widget=forms.TextInput(attrs=message_attrs))
    '''
    def __init__(self):
        super(Status_Form, self).__init__()
        self.fields['message'].widget.attrs['cols'] = 240
    '''