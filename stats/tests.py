from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from status.models import StatusMessage
from friends.models import Friend

# Create your tests here.
class StatsUnitTest(TestCase):
    
    def test_stats_is_exist(self):
        response = Client().get('/stats/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/stats/')
        self.assertEqual(found.func, index)
    
    def test_friends_and_status_count_is_shown(self):
        new_friend = Friend.objects.create(name='Wahyu MR', url ='http://wahyumr.herokuapp.com')
        new_status = StatusMessage.objects.create(message='AAAAAAAAAAAAAAAAAAAA')
        
        response = Client().get('/stats/')
        html_response = response.content.decode('utf8')
        self.assertIn(str(Friend.objects.all().count()), html_response)
        self.assertIn(str(StatusMessage.objects.all().count()), html_response)
    
    def test_last_post_is_shown(self):
        new_status = StatusMessage.objects.create(message='NOOOOOOOOOOOOOOOOOOO')
        
        last_status = StatusMessage.objects.all().last()
        response = Client().get('/stats/')
        html_response = response.content.decode('utf8')
        self.assertIn("NOOOOOOOOOOOOOOOOOOO", html_response)
