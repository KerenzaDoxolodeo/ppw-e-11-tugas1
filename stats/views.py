from django.shortcuts import render
from profiles.views import name
from friends.models import Friend
from status.models import StatusMessage
from profiles.models import Profile

# Create your views here.
def index(request):
    response = {'name': name,
                'feed_count' : StatusMessage.objects.count(),
                'friends_count' : Friend.objects.count(),
                'last_status' : StatusMessage.objects.last()}
    return render(request, 'stats.html', response)
